from django.urls import path, include
from api import views
from rest_framework import routers
app_name = 'api'

router = routers.DefaultRouter()
router.register(r'campaign', views.CrudCampaign)
router.register(r'category', views.CrudCategory)
router.register(r'customer', views.CrudCustomer)
router.register(r'productos', views.CrudProductos)
router.register(r'itemPedido', views.CrudItemPedido)
router.register(r'pedido', views.CrudPedido)



urlpatterns=[
    #cruds
    path('cruds/', include(router.urls)),
    #tienda 
    path('tienda/productos/', views.ListaProductos.as_view(), name="api_tienda_producto"),
    #Cuenta
    path('registro/', views.RegistroAPI.as_view(), name="api_login"),
    path('csrf_cooky/', views.getcsrftoken.as_view(), name="api_token"),
    path('autenticacion/', views.CheckAuthenticatedView.as_view(), name="autenticacion" ),
    path('iniciar_sesion/', views.LoginView.as_view(), name="api_loginn"),
    path('cerrar_sesion/', views.LogoutView.as_view(), name="api_logout"),
    #Usuarios
    path('eliminar_usuario/', views.DeleteAccountView.as_view(), name = "deleteUser"),
    path('ver_usuarios/', views.GetUsersView.as_view(), name = "getUsers"),
    path('ver_usuario/', views.GetCustomerView.as_view(), name = "GetCus"),
    path('actualizar_user/', views.UpdateCustomerProfileView.as_view(), name = "UpdateUser"),
    #Pedidos
    path('ver_pedido/', views.verpedidoAPI.as_view(), name = "apiVerPedido"),
    path('crearpedido/', views.pedidoAPI.as_view(), name = "apiPedido"),

    


    
    ] 